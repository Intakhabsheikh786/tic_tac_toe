

//load the interface on load
const loadInterface = () => {
    //if registered
    if (hasItemToLocalStorage('user')) {
        const home_page = getById('home-page');
        showElement(home_page, 'flex');
        const user = getItemFromLocalStorage('user');

        const welcome_tag = getById('name-tag');
        welcome_tag.innerText = `Hi, ${user.username}`;
    }
    else {
        const starter_page = getById('starter-page');
        showElement(starter_page, 'flex');
        const form = document.querySelector('#new-user-form');
        form.addEventListener('submit', handleNewUserForm);
    }
    //hide the loder
    getById('loader').style.display = 'none';
    getByClassName('container', 0).style.opacity = 1;

}








//form related function

const handleNewUserForm = (e) => {
    e.preventDefault();
    console.log()
    username = e.target[0].value //get the username from the form;
    if (username != '') {
        addUser(username);
        //render username
        const welcome_tag = document.getElementById('name-tag');
        welcome_tag.innerText = username;
        hideElement(document.getElementById('starter-page'));
        showElement(document.getElementById('home-page'), 'flex');
    }
}

const selectGameType = (selected) => {
    if (selected.value == 'local') {
        console.log("playing localy");
    }
    else {
        const div = document.getElementById('online-div');
        showElement(div, 'flex');
    }
}

const handleCreateRoom = () => {
    const user = getItemFromLocalStorage('user');
    const game_id = generateGameId();
    window.location.replace(`${home}game.html?username=${user.username}&game_id=${game_id}`);
}


const handleJoinRoom = () => {
    const joining_div = document.getElementById('joining-div');
    const joining_form = document.querySelector('#joining-form');
    joining_form.addEventListener('submit', handleJoinForm);
    showElement(joining_div, 'flex');

}

const handleJoinForm = (e) => {
    e.preventDefault();
    const game_id = e.target[0].value;
    const user = getItemFromLocalStorage('user');
    window.location.replace(`${home}game.html?username=${user.username}&game_id=${game_id}`);
}



