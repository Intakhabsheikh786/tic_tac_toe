let development = false;

let server_address = development ? 'http://192.168.0.107:7000/' : 'https://tic-tacer-toe.herokuapp.com/';
let home = development ? 'http://192.168.0.107:2000/' : "https://wizardly-mcnulty-84f57f.netlify.app/";





const getById = (id) => document.getElementById(id);
const getByClassName = (name, index) => document.getElementsByClassName(name)[index];

const getQueryString = () => Qs.parse(location.search, { ignoreQueryPrefix: true });

const hideElement = (element) => element.style.display = 'none';
const showElement = (element, type) => element.style.display = type;

const copy = (element) => {
    clipboard = new ClipboardJS(element);
    getById('copy-tag').innerHTML = 'copied';
}

const hasItemToLocalStorage = (item) => localStorage.getItem(item);
const getItemFromLocalStorage = (item) => JSON.parse(localStorage.getItem(item));
const deleteItemFromLocalStorage = (item) => localStorage.removeItem(item);

const generateGameId = () => Math.random().toString(36).slice(2);


const validateQueryKey = (query_string, keys) => {
    const query_string_keys = Object.keys(query_string);
    return keys.every(key => query_string_keys.some(query_string_key => key == query_string_key))
}
const addUser = (username) => {
    const user = { username, logged_at: new Date().getTime() }
    localStorage.setItem('user', JSON.stringify(user));
}

const removeClass = (element, class_name) => element.classList.remove(class_name);
const addClass = (element, class_name) => element.classList.add(class_name);

const createLoader = (element, color) => {
    console.log(color)
    const loading = document.createElement('i');
    loading.style.color = color;
    loading.classList.add('fa', 'fa-circle-o-notch', 'fa-spin');
    element.appendChild(loading);
}

const changeGameRoom = (username, game_id, confirm) => {
    const address = confirm ? `game.html?username=${username}&game_id=${game_id}` : ''
    window.location.replace(`${home}${address}`);

}

const logOut = () => {
    console.log("Yes it is")
    deleteItemFromLocalStorage('user');
    window.location.replace(home)
}


customConfirm = new function () {
    this.confirm = null,
        this.show = (msg, first, second, callback) => {
            const confirmation_div = getByClassName('confirmation', 0);
            const confirm_first = getById('confirm-first');
            const confirm_second = getById('confirm-second');
            confirm_first.innerHTML = first;
            confirm_second.innerHTML = second;
            confirmation_div['firstElementChild'].innerHTML = msg;
            showElement(confirmation_div, 'flex');
            this.callback = callback;
        },
        this.yes = () => {
            const confirm_first = document.querySelector('#confirm-first');
            confirm_first.innerHTML = '';
            const loading = document.createElement('i');
            loading.classList.add('fa', 'fa-circle-o-notch', 'fa-spin');
            confirm_first.appendChild(loading);
            this.callback(true);
            this.confirm = true
            this.close();
        },
        this.no = () => {
            const confirm_second = document.querySelector('#confirm-second');
            confirm_second.innerHTML = '';
            const loading = document.createElement('i');
            loading.classList.add('fa', 'fa-circle-o-notch', 'fa-spin');
            confirm_second.appendChild(loading);
            this.callback(false);
            this.confirm = false

            this.close();
        }
    this.close = () => {
        const confirmation_div = getByClassName('confirmation', 0);
        showElement(confirmation_div, 'none');
    }

}



