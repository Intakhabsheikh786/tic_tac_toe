
let turn;
let mark;
let colors = {
    primary: '#0d568f',
    secondary: '#d62255'
}


const removeClick = (cell_id) => {
    const cell = getById(cell_id);
    cell.removeEventListener('click', handleClick);
}

window.addEventListener("onbeforeunload", function (e) {

    return "Cleaer"

}, false);

let X_CLASS = 'x';
let CIRCLE_CLASS = 'circle';
let WIN_COMB;
let cellElements;

const setup = () => {

    const game_id_tag = getById('game-id-tag');
    const { game_id } = getQueryString();
    game_id_tag.innerHTML = game_id;
    cellElements = document.querySelectorAll('.cell');
    cellElements.forEach(cell => {
        cell.addEventListener('click', handleClick, { once: true })
    })
    X_CLASS = 'x';
    CIRCLE_CLASS = 'circle';
    WIN_COMB = [
        [0, 1, 2],
        [3, 4, 5],
        [6, 7, 8],
        [0, 3, 6],
        [1, 4, 7],
        [2, 5, 8],
        [0, 4, 8],
        [2, 4, 6],
    ]
    getById('loader').style.display = 'none';
    getByClassName('container', 0).style.opacity = 1;
}


//connection
var io = io.connect(server_address);

//get query string
const query_string = getQueryString();

const res = validateQueryKey(query_string, ['username', 'game_id']);
//send the connection to join
if (res) io.emit('join_game', query_string)
else alert("Hello")












//initial game phase

io.on('player_1', res => {
    turn = 'me';
    mark = X_CLASS;
    const starter_page = getById('starter-page');
    showElement(starter_page, 'flex');
})

io.on('player_2', res => {
    turn = 'his';
    mark = CIRCLE_CLASS;

})

io.on('start', res => {
    startGame();
})

function startGame() {
    turnMsg(turn)
    const starter_page = getById('starter-page');
    const game_page = getById('game-page');
    const chat_btn = getByClassName('chat-btn', 0);
    hideElement(starter_page);
    showElement(game_page, 'grid');
    showElement(chat_btn, '')
}




io.on('room_over_flow', msg => {

    const handleConfirmClick = (confirm) => null;
    customConfirm.show(msg, 'dont want to rander', 'dont want to rander', handleConfirmClick);
    const confirm_first = getById('confirm-first');
    const confirm_second = getById('confirm-second');
    hideElement(confirm_first);
    hideElement(confirm_second);
    createLoader(getById('confirmation-btn-div'), colors.secondary);
    setTimeout(() => {
        window.location.replace(`${home}`);
    }, 5000)


})

io.on('user_left', msg => {
    setTimeout(() => {
        const handleConfirmClick = (confirm) => null;
        customConfirm.show(msg, 'dont want to rander', 'dont want to rander', handleConfirmClick);
        const confirm_first = getById('confirm-first');
        const confirm_second = getById('confirm-second');
        hideElement(confirm_first);
        hideElement(confirm_second);
        createLoader(getById('confirmation-btn-div'), colors.secondary);
        setTimeout(() => {
            window.location.replace(`${home}`);
        }, 5000)
    }, 10000)

})







//chat related function

const openChatWindow = () => {
    const chat_div = getByClassName('chat-div', 0);
    showElement(chat_div, 'flex')
}
const hideChatWindow = () => {
    const chat_div = getByClassName('chat-div', 0);
    hideElement(chat_div);
}

const createBubble = (whom, input) => {
    const chat_bubble = document.createElement('div');
    chat_bubble.classList.add(`chat-bubble-${whom}`);
    const msg = document.createElement('label');
    chat_bubble.appendChild(msg);
    msg.textContent = input;
    msg.classList.add('chat-msg');
    const message_div = getByClassName('message-div', 0);
    message_div.appendChild(chat_bubble);

}

io.on('chat-recieve', msg => {
    createBubble('his', msg);
    const chat_div = getByClassName('chat-div', 0);
    showElement(chat_div, 'flex');
})


const sendMessage = () => {
    const chat_input = document.getElementById('chat-input');
    const msg = chat_input.value;
    chat_input.value = '';
    chat_input.focus();
    createBubble('me', msg);
    io.emit('chat-send', msg);
}




const turnMsg = (player) => {
    const turn_msg = getById('turn-msg');
    showElement(turn_msg, '');
    turn_msg.innerHTML = player == 'me' ? 'Its your turn' : "Its opponent turn";
}


const marker = (cell_id, mark) => {
    const cell = getById(cell_id)
    cell.classList.add(mark);
    removeClick(cell_id);
}


const swapTurn = (temp_turn) => {
    turn = temp_turn;
    turnMsg(turn);
}

const isWin = (mark) => WIN_COMB.some(comb => comb.every(index => cellElements[index].classList.contains(mark)))
const isDraw = () => [...cellElements].every(cell => cell.classList.contains(X_CLASS) || cell.classList.contains(CIRCLE_CLASS))



//game won handler function
io.on('challenge', game_id => {
    const handleConfirmClick = (confirm) => {
        const address = confirm ? `game.html?username=${query_string.username}&game_id=${game_id}` : ''
        window.location.replace(`${home}${address}`);
    }
    const confirm_first = getById('confirm-first');
    confirm_first.disabled = false;
    customConfirm.show('The oppenent is asking for challenge', 'Accept', 'Reject', handleConfirmClick);
})

io.on('win', data => {
    const handleConfirmClick = (confirm) => {
        const game_id = generateGameId();
        io.emit('challenge', game_id);
        const address = confirm ? `game.html?username=${query_string.username}&game_id=${game_id}` : ''
        window.location.replace(`${home}${address}`);
    }
    customConfirm.show('Ask for challenge', 'Yes', 'No', handleConfirmClick);
});


//game drawn handler function
io.on('drawn_restart', game_id => {
    const handleConfirmClick = (confirm) => {
        changeGameRoom(query_string.username, game_id, confirm)
    }
    const confirm_first = getById('confirm-first');
    confirm_first.disabled = false;
    customConfirm.show('The Oppenent is asking for replay', 'Yes', 'No', handleConfirmClick);

})

io.on('draw', data => {
    const handleConfirmClick = (confirm) => {
        const game_id = generateGameId();
        io.emit('drawn_restart', game_id);
        changeGameRoom(query_string.username, game_id, confirm)
    }
    customConfirm.show('Game Drawn <br> Do you want to play again ?', 'Yes', 'No', handleConfirmClick);
});



const gameDrawn = () => {
    const handleConfirmClick = (confirm) => {
        const game_id = generateGameId();
        changeGameRoom(query_string.username, game_id, confirm);
    }
    io.emit('draw', 'its drawn');
    const confirm_first = getById('confirm-first');
    confirm_first.disabled = true;
    customConfirm.show('Game Drawn <br> Do you want to play again ?', '', 'No', handleConfirmClick);
    createLoader(confirm_first, 'white');
}

const gameWon = () => {
    const handleConfirmClick = (confirm) => {
        const game_id = generateGameId();
        changeGameRoom(query_string.username, game_id, confirm);
    }
    io.emit('win', 'Opponent win');
    const confirm_first = getById('confirm-first');
    confirm_first.disabled = true;
    customConfirm.show('You are the winner <br> Do you want to play again', '', 'No', handleConfirmClick);
    createLoader(confirm_first, 'white');
}




const endGame = (draw) => {
    if (draw) gameDrawn();
    else gameWon();
}



io.on('clicked', data => {
    console.log("Heloo")
    if (turn == 'his') {
        const game_page = getById('game-page');
        removeClass(game_page, 'disable')
        marker(data.cell_id, data.mark);
        swapTurn('me');
    }
})

const handleClick = (e) => {
    if (turn == 'me') {
        const game_page = getById('game-page');
        addClass(game_page, 'disable');
        swapTurn('his');
        const { id: cell_id } = e.target;
        let data = { cell_id, mark }
        marker(cell_id, mark);
        const win = isWin(mark);
        if (win) endGame(false);
        else if (isDraw()) endGame(true);
        io.emit('clicked', data);
    }
}











